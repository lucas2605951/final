FROM ubuntu:latest
LABEL version="0.1"
LABEL description="trabajo final" 
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y nginx  
ADD https://gitlab.com/lucas2605951/final/-/raw/main/index.html  /var/www/html
RUN chmod +r /var/www/* -R
ADD https://gitlab.com/lucas2605951/final/-/raw/main/default.conf  /etc/nginx/sites-available
CMD ["nginx", "-g", "daemon off;"]
EXPOSE 8080
 





